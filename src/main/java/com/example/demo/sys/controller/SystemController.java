package com.example.demo.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 系统打开窗口
 */
@Controller
@RequestMapping("sys")
public class SystemController {

	/**
	 *跳转主页
	 */
	@RequestMapping("index")
	public String index(){
		return "sys/index";
	}

	/**
	 *加载首页的初始化窗口
	 */
	@RequestMapping("toDeskManager")
	public String toDeskManager(){
		return "sys/toDeskManager";
	}

	/**
	 *无权限访问页面
	 */
	@RequestMapping("unAuth")
	public String unAuth(){
		return "sys/error/unAuth";
	}

	/**
	 *加载菜单
	 */
	@RequestMapping("toMenuManager")
	public String menuList(){
		return "sys/menu/menuManager";
	}

	/**
	 *加载菜单管理的左菜单树,
	 */
	@RequestMapping("toMenuLeft")
	public String menuLeftList(){
		return "sys/menu/menuLeft";
	}

	/**
	 *加载菜单管理的右列表,
	 */
	@RequestMapping("toMenuRight")
	public String menuRightList(){
		return "sys/menu/menuRight";
	}

	/**
	 *加载菜单
	 */
	@RequestMapping("toDeptManager")
	public String toDeptManager(){
		return "sys/dept/deptManager";
	}

	/**
	 *加载部门管理的左菜单树,
	 */
	@RequestMapping("toDeptLeft")
	public String toDeptLeft(){
		return "sys/dept/deptLeft";
	}

	/**
	 *加载部门管理的右列表,
	 */
	@RequestMapping("toDeptRight")
	public String toDeptRight(){
		return "sys/dept/deptRight";
	}

	/**
	 *加载用户管理界面
	 */
	@RequestMapping("toUserManager")
	public String toUserManager(){
		return "sys/user/userManager";
	}

	/**
	 *加载角色管理界面
	 */
	@RequestMapping("toRoleManager")
	public String toRoleManager(){
		return "sys/role/roleManager";
	}

	/**
	 *加载权限管理界面
	 */
	@RequestMapping("toPermissionManager")
	public String toPermissionManager(){
		return "sys/permission/permissionManager";
	}

	/**
	 *加载权限管理界面
	 */
	@RequestMapping("toJobManager")
	public String toJobManager(){
		return "sys/job/jobManager";
	}
}
