package com.example.demo.sys.controller;

import com.example.demo.sys.domain.User;
import com.example.demo.sys.form.UserForm;
import com.example.demo.sys.service.UserService;
import com.example.demo.sys.utils.JsonModel;
import com.example.demo.sys.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService userService;

	/**
	 *跳转用户列表
	 */
	@RequestMapping("userList")
	public String admin(){
    	return "user/userList";
	}

	/**
	 *用户列表
	 */
	@RequestMapping("admin")
	@ResponseBody
	public JsonModel admin(UserForm userForm, HttpSession session){
		JsonModel jsonModel = new JsonModel();

		//查询当前用户的username
		String currentUserName = (String) session.getAttribute("userName");

		//查询所有用户
		List<UserVO> users = userService.findAll(userForm);

		//绑定数据
		session.setAttribute("users",users);
		jsonModel.setData(users);
		jsonModel.setCode(0);
		jsonModel.setCount((long) users.size());
		return jsonModel;
	}
	/**
	 *删除单个用户
	 */
	@RequestMapping("delete")
	@ResponseBody
	public JsonModel delete(Integer id){
		JsonModel jsonModel = new JsonModel();
		//先删除与职位表的关联
		userService.deleteUserJob(id);
		//删除与角色表的关联
		userService.deleteUserRole(id);
		//删除用户信息
		userService.delete(id);
		jsonModel.setCode(0);
		jsonModel.setMsg("删除成功！");
		return jsonModel;
	}
	/**
	 *批量删除
	 */
	@RequestMapping(value = "batchDelete", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel batchDelete(String ids){
		JsonModel jsonModel = new JsonModel();
		//先删除中间表的关联
		System.out.println("ids:"+ids);
		String[] userIds = ids.split(",");
		for (String id : userIds) {
			//先删除与职位表的关联
			userService.deleteUserJob(Integer.parseInt(id));
			//删除与角色表的关联
			userService.deleteUserRole(Integer.parseInt(id));
			//删除用户信息
			userService.delete(Integer.parseInt(id));
		}
		jsonModel.setCode(0);
		jsonModel.setMsg("删除成功！");
		return jsonModel;
	}

	/**
	 *添加用户
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel add(UserForm userForm){
		JsonModel jsonModel = new JsonModel();
		//判断是否已经存在该用户
		List<User> users = userService.findNameOrLoginName(userForm.getName(), userForm.getLoginName());
		if (users.size() != 0){
			jsonModel.setMsg("添加失败！！姓名或登录名已存在，请更换！");
			return jsonModel;
		}
		//添加用户信息
		userService.add(userForm);
		//绑定职位关系
		userService.bandUserJob(userForm);
		//绑定角色关系
		userService.bandUserRole(userForm);

		jsonModel.setMsg("添加成功");
		jsonModel.setCode(0);
		return jsonModel;
	}

	/**
	 *修改用户信息
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel update(UserForm userForm ,Integer sex){
		JsonModel jsonModel = new JsonModel();
		userForm.setSex(sex);
		//判断是否已经存在该用户
		List<User> users = userService.findNameOrLoginName(userForm.getName(), userForm.getLoginName());
		if (users.size() != 0){
			for (User dbUser : users){
				//判断相同的用户是否为自己，若是自己则可以修改，不是则返回false
				if(dbUser.getId() != userForm.getId()){
					jsonModel.setMsg("更新失败！！姓名或登录名已存在，请重新更改！");
					return jsonModel;
				}
			}
		}
		if(userForm.getJob().equals("undefined") || "".equals(userForm.getJob()) ){
			System.out.println("userForm.getJob():"+userForm.getJob());
		}else{
			//先删除与职位表的关联
			userService.deleteUserJob(userForm.getId());
			//添加职位关联关系
			userService.bandUserJob(userForm);
		}

		//删除与角色表的关联
		userService.deleteUserRole(userForm.getId());
		//更新用户信息
		userService.update(userForm);
		//添加角色关联关系
		userService.bandUserRole(userForm);
		jsonModel.setCode(0);
		jsonModel.setMsg("更新成功");
		return jsonModel;
	}
}
