package com.example.demo.sys.controller;

import com.example.demo.sys.domain.Dept;
import com.example.demo.sys.form.DeptForm;
import com.example.demo.sys.service.DeptService;
import com.example.demo.sys.service.UserService;
import com.example.demo.sys.utils.JsonModel;
import com.example.demo.sys.utils.TreeNode;
import com.example.demo.sys.vo.DeptVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("dept")
public class DeptController {

	@Autowired
	private DeptService deptService;

	@Autowired
	private UserService userService;

	/**
	 *加载左边管理左边菜单树,封装返回json
	 */
	@RequestMapping("loadDeptManagerLeftTreeJson")
	@ResponseBody
	public JsonModel loaddeptManagerLeftTreeJson(){
		JsonModel jsonModel = new JsonModel();
		List<Dept> depts = deptService.findAll(new DeptVO());

		//封装成树结构
		List<TreeNode> treeNodes = new ArrayList<TreeNode>();
		for (Dept dept : depts) {
			Integer id = dept.getId();
			Integer pid = dept.getPid();
			String title = dept.getTitle();
			Boolean spread = dept.getOpen()==1?true:false;
			treeNodes.add(new TreeNode(id, pid, title, spread));
		}
		jsonModel.setCode(0);
		jsonModel.setData(treeNodes);
		return jsonModel;
	}

	/**
	 * 查询
	 */
	@RequestMapping("loadAllDept")
	@ResponseBody
	public JsonModel loadAllDept(DeptVO deptVO){
		JsonModel jsonModel = new JsonModel();
		List<Dept> depts = deptService.findAll(deptVO);
		for (Dept dept : depts){
			if(dept.getId()==-1){
				depts.remove(dept);
				break;
			}
		}
		jsonModel.setCode(0);
		jsonModel.setCount((long) depts.size());
		jsonModel.setData(depts);
		return jsonModel;
	}

	/**
	 *添加部门信息
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel add(DeptForm deptForm){
		JsonModel jsonModel = new JsonModel();
		if(deptForm.getDeptId()==null){
			deptForm.setDeptId(0);
		}
		//先判断部门是否已存在
		List<Dept> depts = deptService.findAll(new DeptVO());
		for (Dept dept:depts){
			if(dept.getTitle().equals(deptForm.getTitle())){
				jsonModel.setMsg("失败！该部门已存在请重新添加");
				return jsonModel;
			}
		}
		//添加部门信息
		deptService.add(deptForm);
		//绑定职位
		if(!"".equals(deptForm.getJob())){
			//不为空才添加
			String[] jobIds = deptForm.getJob().split(",");
			for (String jobId : jobIds){
				deptService.bandJob(deptForm.getId(),Integer.parseInt(jobId));
			}
		}
		jsonModel.setMsg("添加部门成功！");
		jsonModel.setCode(0);
		return jsonModel;
	}

	/**
	 *删除单个部门
	 */
	@RequestMapping("delete")
	@ResponseBody
	public JsonModel delete(Integer id){
		JsonModel jsonModel = new JsonModel();
		//先删除与职位表的关联
		deptService.deleteBandJob(id);
		//更新该部门的员工信息，解除该部门员工与该部门的关系
		userService.updateDeptId(id);
		//与子部门解绑部门关系
		deptService.updatePid(id);
		//删除部门信息
		deptService.delete(id);
		jsonModel.setCode(0);
		jsonModel.setMsg("删除成功！");
		return jsonModel;
	}
	/**
	 *修改部门信息
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel update(DeptForm deptForm){
		JsonModel jsonModel = new JsonModel();

		//判断是否已经存在该部门
		List<Dept> depts = deptService.findAll(new DeptVO());
		for(Dept dept: depts){
			if(dept.getTitle().equals(deptForm.getTitle()) && dept.getId() != deptForm.getId()){
				jsonModel.setMsg("失败！该部门名字已存在请重新修改！");
				return jsonModel;
			}
		}
		if(deptForm.getDeptId()==null){
			deptForm.setDeptId(0);
		}
		//更新部门信息
		deptService.update(deptForm);
		//删除绑定职位信息
		deptService.deleteBandJob(deptForm.getId());
		//绑定职位绑定关系
		if(!"".equals(deptForm.getJob())){
			//不为空才添加
			String[] jobIds = deptForm.getJob().split(",");
			for (String jobId : jobIds){
				deptService.bandJob(deptForm.getId(),Integer.parseInt(jobId));
			}
		}

		jsonModel.setCode(0);
		jsonModel.setMsg("更新成功");
		return jsonModel;
	}
}
