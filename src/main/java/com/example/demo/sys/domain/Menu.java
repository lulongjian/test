package com.example.demo.sys.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Menu {
	private Integer id;
	private Integer pid;	//父节点
	private String title;	//菜单名字
	private String icon;	//图标
	private String href;	//跳转路劲
	private Integer open;	//0代表父节点 1代表子节点

}
