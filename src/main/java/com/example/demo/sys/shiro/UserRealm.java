package com.example.demo.sys.shiro;

import com.example.demo.sys.domain.User;
import com.example.demo.sys.service.UserService;
import com.example.demo.sys.utils.Constant;
import com.example.demo.sys.utils.RASEncrypt;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 自定义Realm
 * @author
 */
public class UserRealm extends AuthorizingRealm {

	@Autowired
	private UserService userService;

	/**
	 *执行授权逻辑
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
		System.out.println("执行授权逻辑");

		//给资源进行授权
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

		//添加资源授权字符串
		//info.addStringPermission("");

		//到数据库查询当前登陆用户的的授权字符串
		//获取当前登陆用户
		Subject subject = SecurityUtils.getSubject();
		User user = (User)subject.getPrincipal();
		List<String> permission = userService.findPermissionByUserId(user.getId());
		info.addStringPermissions(permission);

		return info;
	}

	/**
	 *执行认证逻辑
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
		System.out.println("执行认证逻辑");

		//编写Shiro判断逻辑，判断用户名和密码
		//1.判断用户名
		UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
		User user = userService.findByloginName(token.getUsername());
		//RAS解密
		try {
			user.setPassword(RASEncrypt.decrypt(user.getPassword(), Constant.PRIVATEKRY));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(user==null){
			//用户名不存在
			return null;//shiro底层会抛出UnknownAccountException异常
		}
		//取出盐并编码
		//判断密码

		return  new SimpleAuthenticationInfo(user, user.getPassword(),"");
	}
}
