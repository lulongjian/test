package com.example.demo.sys.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SelectJsonVO {
	private String name;
	private Integer value;
	private String selected;
	private String disabled;

	public SelectJsonVO(String name, Integer value, String selected, String disabled) {
		this.name = name;
		this.value = value;
		this.selected = selected;
		this.disabled = disabled;
	}

}
