package com.example.demo.sys.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PermissionVO {
	private Integer id;
	private String title;
	private String href;
	private String permission;
	private String name;
	private Integer value;
	private String selected;
	private String disabled;

	//构造多选数据格式构造器
	public PermissionVO(String name, Integer value, String selected, String disabled) {
		this.name = name;
		this.value = value;
		this.selected = selected;
		this.disabled = disabled;
	}
}
