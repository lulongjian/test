package com.example.demo.sys.vo;

import com.example.demo.sys.domain.Role;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RoleVO extends Role {
	private String roleNo;
	private String role;
}
