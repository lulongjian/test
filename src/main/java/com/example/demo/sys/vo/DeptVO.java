package com.example.demo.sys.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DeptVO {
	private Integer id; //当前节点id
	private String title; //部门名字
	private String deptNo;//部门编号
	private String parentDeptNo;	//上级部门编号
	private String parentDept;//上级部门
}
