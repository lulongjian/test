package com.example.demo.sys.dao;

import com.example.demo.sys.domain.Permission;
import com.example.demo.sys.form.PermissionForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PermissionMapper {

	List<Permission> findAll(PermissionForm permissionForm);//查找所有的权限

	int add(PermissionForm permissionForm); //添加权限信息

	int bandRole(@Param("permissionId") Integer permissionId, @Param("roleId") Integer roleId); //绑定角色与权限的关系

	int delete(Integer id);//删除权限

	int deleteRole(Integer permissionId);//删除与角色绑定的关系
}
