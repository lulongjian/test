package com.example.demo.sys.service.impl;

import com.example.demo.sys.dao.DeptMapper;
import com.example.demo.sys.domain.Dept;
import com.example.demo.sys.form.DeptForm;
import com.example.demo.sys.service.DeptService;
import com.example.demo.sys.utils.FieldNumber;
import com.example.demo.sys.vo.DeptVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {

	@Autowired
	private DeptMapper deptMapper;

	/**
	 * 查找所有部门
	 */
	@Override
	public List<Dept> findAll(DeptVO deptVO) {
		return deptMapper.findAll(deptVO);
	}

	/**
	 *添加部门信息
	 */
	@Override
	public int add(DeptForm deptForm) {

		//设置部门的编号
		List<Dept> depts = deptMapper.findAll(new DeptVO());
		String deptNo = depts.get(depts.size()-1).getDeptNo();
		deptForm.setDeptNo(FieldNumber.number(Integer.parseInt(deptNo)));

		//时间设置
		deptForm.setCreateTime(new Date());
		deptForm.setUpdateTime(new Date());
		deptForm.setOpen(0);
		return deptMapper.add(deptForm);
	}

	/**
	 *绑定部门的职位
	 */
	@Override
	public int bandJob(Integer deptId, Integer jobId) {
		return deptMapper.bandJob(deptId, jobId);
	}

	/**
	 *删除与职位的绑定关系
	 */
	@Override
	public int deleteBandJob(Integer deptId) {
		return deptMapper.deleteBandJob(deptId);
	}

	/**
	 *通过id删除部门
	 */
	@Override
	public int delete(Integer id) {
		return deptMapper.delete(id);
	}

	@Override
	public int update(DeptForm deptForm) {
		//设置更新时间
		deptForm.setUpdateTime(new Date());
		return deptMapper.update(deptForm);
	}

	@Override
	public int updatePid(Integer pid) {
		return deptMapper.updatePid(pid);
	}
}
