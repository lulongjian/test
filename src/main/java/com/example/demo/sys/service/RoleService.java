package com.example.demo.sys.service;


import com.example.demo.sys.domain.Permission;
import com.example.demo.sys.form.RoleForm;
import com.example.demo.sys.vo.RoleVO;

import java.util.List;

public interface RoleService {

	List<RoleVO> findAll(RoleForm roleForm);//查找所有的角色信息

	int add(RoleForm roleForm); //添加角色

	int bandPermission(int roleId,Integer permissionId); //绑定权限

	int delete(Integer id);//删除角色

	int deletePermission(Integer id);//删除角色的权限关系

	int deleteBandUser(Integer id);//删除角色的用户关系

	int update(RoleForm roleForm);//修改角色信息
}
