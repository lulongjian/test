package com.example.demo.sys.service.impl;


import com.example.demo.sys.dao.UserMapper;
import com.example.demo.sys.domain.User;
import com.example.demo.sys.form.UserForm;
import com.example.demo.sys.service.UserService;
import com.example.demo.sys.utils.Constant;
import com.example.demo.sys.utils.FieldNumber;
import com.example.demo.sys.utils.RASEncrypt;
import com.example.demo.sys.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;

	@Override
	public User findByloginName(String userName) {
		User user = userMapper.findByloginName(userName);
		return user;
	}

	@Override
	public List<UserVO> findAll(UserForm userForm) {
		return userMapper.findAll(userForm);
	}

	@Override
	public int delete(Integer id) {
		return userMapper.delete(id);
	}

	@Override
	public int deleteUserRole(Integer userId) {
		return userMapper.deleteUserRole(userId);
	}

	@Override
	public int deleteUserJob(Integer userId) {
		return userMapper.deleteUserJob(userId);
	}

	@Override
	public List<User> findNameOrLoginName(String name, String loginName) {
		return userMapper.findNameOrLoginName(name, loginName);
	}

	@Override
	public int add(UserForm userForm) {
		// 对密码进行加密
		try {
			Map<Integer,String> keyMap = RASEncrypt.genKeyPair();
			String password = RASEncrypt.encrypt("123456", Constant.PUBLICKRY);
			userForm.setPassword(password);
		} catch (Exception e) {
			e.printStackTrace();
		}

		//添加员工编号
		List<UserVO> userVOS = userMapper.findAll(new UserForm());
		//获取员工列表最后一个员工编号
		String userNo = userVOS.get(userVOS.size()-1).getUserNo();
		userForm.setUserNo(FieldNumber.number(Integer.parseInt(userNo)));

		userForm.setCreateTime(new Date());
		userForm.setUpdateTime(new Date());
		return userMapper.add(userForm);
	}

	@Override
	public int bandUserJob(UserForm userForm) {
		if(userForm.getJob().equals("undefined") || "".equals(userForm.getJob()) ){
			return 0;
		}
		userForm.setJobId(Integer.parseInt(userForm.getJob()));
		return userMapper.bandUserJob(userForm);
	}

	@Override
	public int bandUserRole(UserForm userForm) {
		return userMapper.bandUserRole(userForm);
	}

	@Override
	public int update(UserForm userForm) {
		//设置更新时间为当前时间
		userForm.setUpdateTime(new Date());
		return userMapper.update(userForm);
	}

	/**
	 * 更新关系部门，删除部门是解绑关系
	 */
	@Override
	public int updateDeptId(Integer deptId) {
		return userMapper.updateDeptId(deptId);
	}

	@Override
	public List<String> findPermissionByUserId(Integer id) {
		return userMapper.findPermissionByUserId(id);
	}

}
