package com.example.demo.sys.service;

import com.example.demo.sys.domain.Dept;
import com.example.demo.sys.domain.Job;
import com.example.demo.sys.form.DeptForm;
import com.example.demo.sys.form.UserForm;
import com.example.demo.sys.vo.DeptVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DeptService {

	List<Dept> findAll(DeptVO deptVO);	//查找所有部门

	int add(DeptForm deptForm);	//添加部门信息

	int bandJob(Integer deptId, Integer jobId);//绑定职位信息

	int deleteBandJob(Integer deptId);//删除与职位绑定关系

	int delete(Integer id);//通过id删除部门

	int update(DeptForm deptForm);//更新部门信息

	int updatePid(Integer pid);//更新部门的上级id
}
