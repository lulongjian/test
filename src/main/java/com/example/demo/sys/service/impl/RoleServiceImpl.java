package com.example.demo.sys.service.impl;

import com.example.demo.sys.dao.RoleMapper;
import com.example.demo.sys.form.RoleForm;
import com.example.demo.sys.service.RoleService;
import com.example.demo.sys.utils.FieldNumber;
import com.example.demo.sys.vo.RoleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleMapper roleMapper;

	@Override
	public List<RoleVO> findAll(RoleForm roleForm) {
		return roleMapper.findAll(roleForm);
	}

	@Override
	public int add(RoleForm roleForm) {
		//获取最后一个角色编号
		List<RoleVO> roleVOS = roleMapper.findAll(new RoleForm());
		String roleNo = roleVOS.get(roleVOS.size()-1).getRoleNo();
		roleForm.setRoleNo(FieldNumber.number(Integer.parseInt(roleNo)));
		roleForm.setCreateTime(new Date());
		roleForm.setUpdateTime(new Date());
		return roleMapper.add(roleForm);
	}

	@Override
	public int bandPermission(int roleId,Integer permissionId) {
		return roleMapper.bandPermission(roleId,permissionId);
	}

	@Override
	public int delete(Integer id) {
		return roleMapper.delete(id);
	}

	@Override
	public int deletePermission(Integer id) {
		return roleMapper.deletePermission(id);
	}

	@Override
	public int deleteBandUser(Integer id) {
		return roleMapper.deleteBandUser(id);
	}

	@Override
	public int update(RoleForm roleForm) {
		roleForm.setUpdateTime(new Date());
		return roleMapper.update(roleForm);
	}
}
