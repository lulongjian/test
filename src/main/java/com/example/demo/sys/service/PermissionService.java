package com.example.demo.sys.service;


import com.example.demo.sys.domain.Permission;
import com.example.demo.sys.form.PermissionForm;
import com.example.demo.sys.vo.PermissionVO;

import java.util.List;

public interface PermissionService {

	List<Permission> findAll(PermissionForm permissionForm);//查找所有的权限

	int add(PermissionForm permissionForm); //添加权限信息

	int bandRole(Integer id, Integer roleId); //绑定角色与权限的关系

	int delete(Integer id);//删除权限

	int deleteRole(Integer permissionId);//删除与角色绑定的关系
}
