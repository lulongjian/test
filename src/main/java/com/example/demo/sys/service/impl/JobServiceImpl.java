package com.example.demo.sys.service.impl;

import com.example.demo.sys.dao.JobMapper;
import com.example.demo.sys.dao.RoleMapper;
import com.example.demo.sys.form.JobForm;
import com.example.demo.sys.service.JobService;
import com.example.demo.sys.service.RoleService;
import com.example.demo.sys.vo.JobVO;
import com.example.demo.sys.vo.RoleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class JobServiceImpl implements JobService {

	@Autowired
	private JobMapper jobMapper;


	@Override
	public List<JobVO> findAll(JobForm jobForm) {
		return jobMapper.findAll(jobForm);
	}

	@Override
	public JobVO findJobByJobName(String title) {
		return jobMapper.findJobByJobName(title);
	}

	@Override
	public int add(JobForm jobForm) {
		jobForm.setCreateTime(new Date());
		jobForm.setUpdateTime(new Date());
		return jobMapper.add(jobForm);
	}

	@Override
	public int deleteBandUser(Integer jobId) {
		return jobMapper.deleteBandUser(jobId);
	}

	@Override
	public int deleteBandDept(Integer jobId) {
		return jobMapper.deleteBandDept(jobId);
	}

	@Override
	public int delete(Integer id) {
		return jobMapper.delete(id);
	}

	@Override
	public int update(JobForm jobForm) {
		jobForm.setUpdateTime(new Date());
		return jobMapper.update(jobForm);
	}
}
