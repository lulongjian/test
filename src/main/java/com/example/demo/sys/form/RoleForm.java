package com.example.demo.sys.form;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
public class RoleForm {
	private Integer id;
	private String roleNo;
	private String title;
	private Integer permissionId;
	private Date createTime; //添加时间
	private Date updateTime; //更新时间

}
