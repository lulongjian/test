package com.example.demo.sys.form;

import com.example.demo.sys.domain.Dept;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DeptForm extends Dept {
	private Integer deptId; //上级部门id
	private String job;	//职位字符串
	private String deptNo;//部门编号
	private String parentDeptNo;	//上级部门编号
}
